<?php

$action=filter_var($_GET["action"]??"home",FILTER_SANITIZE_FULL_SPECIAL_CHARS);

switch ($action){
    case "home":
        require('view/home.php');
    break;

    case "contact" :
        require ('view/contact.php');
    break;

 

    case "formLogin":
        
        require("view/client/login.php");
    break;

    case "traitementLogin":
        var_dump($_POST);
        $email=filter_var($_POST['email'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp=filter_var($_POST['mdp'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp=hash("sha256",$mdp);
      
        $objetClientManager=new ClientManager($lePDO);
        $client=$objetClientManager->fetchClientByEmailAndMdp($email,$mdp);
        if(empty($client)){
            //msg $msgErreur
            $_SESSION['erreur']= [];
            array_push($_SESSION['erreur'],"Erreur connexion");
            header("location:./?path=main&action=formLogin");
        }
        else{
            $_SESSION["email"]=$client->getEmail();
            $_SESSION["id"]=$client->getIdClient();
            $_SESSION["role"]="client";
            header("location:./");
      
        }
        
    break;

    case "logout":
        session_unset();
        session_destroy();
        header("location:./?path=main&action=formLogin");
        break;

    case "formInscription":
        require("view/client/inscription.php");
    break;
    
    case "traitementInscription" :
        //@todo 
    break;

    default :
    require('view/404.php');
}

?>